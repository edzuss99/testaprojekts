<?php

namespace src;

include 'IndexHandler.php';
include 'DocumentReader.php';

class IndexService
{
    protected DocumentReader $document;

    protected IndexHandler $indexHandler;

    public function __construct()
    {
        $this->document = new DocumentReader();
        $this->indexHandler = new IndexHandler();
    }

    public function handleOption(string $option)
    {
        switch ($option) {
            case 1:
                $this->processQueryOption();
                break;
            case 2:
                $this->processManualOption();
                break;
            case 3:
                $this->processAndOption();
                break;
            case 4:
                $this->processOrOption();
                break;
            case 5:
                $this->processAllOptions();
                break;
            default:
                die('INVALID OPTION: Please choose option between 1 and 5');
        }
    }

    private function processQueryOption()
    {
        $filename = readline('Please enter dictionary file name: ');
        $this->document->readDocumentContents($filename);
        $this->indexHandler->handleDictionary($this->document->getDocumentContent());

        $filename = readline('Please enter query file name: ');
        $this->document->readDocumentContents($filename);
        $this->indexHandler->handleQuery($this->document->getDocumentContent());

        $this->indexHandler->getPostings();

        $filename = readline('Please enter name of the file where you want to save the results: ');

        $this->document->saveResults($filename, $this->indexHandler);
    }

    private function processManualOption()
    {
        $filename = readline('Please enter dictionary file name: ');
        $this->document->readDocumentContents($filename);
        $this->indexHandler->handleDictionary($this->document->getDocumentContent());

        $terms = readline('Please enter terms which you want to search - separate by spaces: ');
        $this->indexHandler->handleManualTerms($terms);
        $this->indexHandler->getPostings();

        $filename = readline('Please enter name of the file where you want to save the results: ');

        $this->document->saveResults($filename, $this->indexHandler);
    }

    private function processAndOption()
    {
        $filename = readline('Please enter dictionary file name: ');
        $this->document->readDocumentContents($filename);
        $this->indexHandler->handleDictionary($this->document->getDocumentContent());

        $filename = readline('Please enter query file name: ');
        $this->document->readDocumentContents($filename);
        $this->indexHandler->handleQuery($this->document->getDocumentContent());
        $this->indexHandler->queryAnd();

        $filename = readline('Please enter name of the file where you want to save the results: ');

        $this->document->saveAndQuery($filename, $this->indexHandler);
    }

    private function processOrOption()
    {
        $filename = readline('Please enter dictionary file name: ');
        $this->document->readDocumentContents($filename);
        $this->indexHandler->handleDictionary($this->document->getDocumentContent());

        $filename = readline('Please enter query file name: ');
        $this->document->readDocumentContents($filename);
        $this->indexHandler->handleQuery($this->document->getDocumentContent());
        $this->indexHandler->queryOr();

        $filename = readline('Please enter name of the file where you want to save the results: ');

        $this->document->saveOrQuery($filename, $this->indexHandler);
    }

    private function processAllOptions()
    {
        $filename = readline('Please enter dictionary file name: ');
        $this->document->readDocumentContents($filename);
        $this->indexHandler->handleDictionary($this->document->getDocumentContent());

        $filename = readline('Please enter query file name: ');
        $this->document->readDocumentContents($filename);
        $this->indexHandler->handleQuery($this->document->getDocumentContent());

//        $filename = readline('Please enter bi-gram file name: ');
//        $this->document->readDocumentContents($filename);
//        $this->indexHandler->handleBigramQuery($this->document->getDocumentContent());
//        $this->indexHandler->getAllData();

        $filename = readline('Please enter replacement query file name: ');
        $this->document->readDocumentContents($filename);
        $this->indexHandler->handleWildCardQuery($this->document->getDocumentContent());
        $this->indexHandler->getAllData();

        $filename = readline('Please enter name of the file where you want to save the results: ');

        $this->document->saveEverything($filename, $this->indexHandler);
    }


}