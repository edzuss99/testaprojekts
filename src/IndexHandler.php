<?php


namespace src;


class IndexHandler
{

    public array $dictionary = [];
    public array $document = [];
    public array $query = [];
    public array $bigram = [];
    public array $wildcard = [];
    public int $documentCount = 0;
    public array $queryIndexWords = [];

    public function getPostings(): void
    {
        if (empty($this->dictionary)) {
            die('Dictionary is empty, please provide text!');
        }

        foreach ($this->query as $termRow) {
            echo 'Get Postings' . "\r\n";
            foreach ((array)$termRow as $term) {
                echo trim($term) . "\r\n";
                $term = trim($term);
                if (isset($this->dictionary[$term])) {
                    $wordIndexRow = '';
                    foreach ($this->dictionary[$term] as $index) {
                        $wordIndexRow = $wordIndexRow . " $index";
                    }
                    echo 'Postings list : ' . $wordIndexRow . "\r\n";
                }
            }
            echo "\r\n";
        }

    }

    public function queryAnd(): void
    {
        if (empty($this->dictionary)) {
            die('Dictionary is empty, please provide text!');
        }


        foreach ($this->query as $termRow) {
            $queryDictionary = [];
            $termString = '';
            echo "QueryAnd" . "\r\n";
            foreach ((array)$termRow as $term) {
                $term = trim($term);
                $termString = $termString . $term . ' ';
                if (isset($this->dictionary[$term])) {
                    $queryDictionary[$term] = $this->dictionary[$term];
                }
            }

            echo $termString . "\r\n";
            if (count($queryDictionary) == 1) {
                foreach ($queryDictionary as $documentId) {
                    echo "Results: " . implode($documentId) . "\r\n";
                    break;
                }
                continue;
            }

            $intersectedDictionary = call_user_func_array('array_intersect', $queryDictionary);

            if (!empty($intersectedDictionary)) {
                $intersectedIdCollectionString = '';
                foreach ($intersectedDictionary as $intersectedId) {
                    $intersectedIdCollectionString = $intersectedIdCollectionString . $intersectedId . ' ';
                }
                echo "Results: $intersectedIdCollectionString" . "\r\n";
            } else {
                echo "Results: empty" . "\r\n";
            }

        }
    }

    public function queryOr(): void
    {
        if (empty($this->dictionary)) {
            die('Dictionary is empty, please provide text!');
        }

        foreach ($this->query as $termRow) {
            $queryDictionary = [];
            $termString = '';
            echo "QueryOr" . "\r\n";
            foreach ((array)$termRow as $term) {
                $term = trim($term);
                $termString = $termString . $term . ' ';
                if (isset($this->dictionary[$term])) {
                    $queryDictionary[$term] = $this->dictionary[$term];
                }
            }
            echo $termString . "\r\n";

            $mergedArray = call_user_func_array('array_merge', $queryDictionary);

            $mergedArrayIds = [];
            foreach ($mergedArray as $key => $id) {
                $mergedArrayIds[] = (int)$id;
            }

            sort($mergedArrayIds);
            $mergedArrayIds = array_unique($mergedArrayIds);
            $mergedString = '';
            foreach ($mergedArrayIds as $id) {
                $mergedString = $mergedString . $id . ' ';
            }

            echo "Results: $mergedString \r\n";
        }
    }

    public function getAllData()
    {
        if (empty($this->dictionary)) {
            die('Dictionary is empty, please provide text!');
        }

        foreach ($this->query as $termRow) {
            echo 'Get Postings' . "\r\n";
            foreach ((array)$termRow as $term) {
                echo trim($term) . "\r\n";
                $term = trim($term);
                if (isset($this->dictionary[$term])) {
                    $wordIndexRow = '';
                    foreach ($this->dictionary[$term] as $index) {
                        $wordIndexRow = $wordIndexRow . " $index";
                    }
                    echo 'Postings list : ' . $wordIndexRow . "\r\n";
                }
            }


            $queryDictionary = [];
            $termString = '';
            echo "QueryAnd" . "\r\n";
            foreach ((array)$termRow as $term) {
                $term = trim($term);
                $termString = $termString . $term . ' ';
                if (isset($this->dictionary[$term])) {
                    $queryDictionary[$term] = $this->dictionary[$term];
                }
            }
            $uniqueValues = $this->getUniqueQueryIndexes($termRow);
            $this->getQueryIndexWords($uniqueValues, $queryDictionary);
            $tfIdfArray = $this->processTFIDF();
            asort($tfIdfArray);
            $tfIdfArray = array_reverse($tfIdfArray, true);
            echo $termString . "\r\n";
            if (count($queryDictionary) == 1) {
                foreach ($queryDictionary as $documentId) {
                    echo "Results: " . implode($documentId) . "\r\n";
                    echo "TF-IDF \r\n";
                    echo "Results: " . implode($documentId) . "\r\n";
                    break;
                }
            } else {
                $intersectedDictionary = call_user_func_array('array_intersect', $queryDictionary);
                if (!empty($intersectedDictionary)) {
                    $intersectedIdCollectionString = '';
                    foreach ($intersectedDictionary as $intersectedId) {
                        $intersectedIdCollectionString = $intersectedIdCollectionString . $intersectedId . ' ';
                    }
                    echo "Results: $intersectedIdCollectionString" . "\r\n";

                    echo "TF-IDF \r\n";
                    $tfidfString = '';
                    foreach ($tfIdfArray as $index => $value) {
                        if (in_array($index, $intersectedDictionary)) {
                            $tfidfString = $tfidfString . $index . ' ';
                        }
                    }
                    echo "Results: $tfidfString \r\n";

                } else {
                    echo "Results: empty" . "\r\n";
                    echo "TF-IDF \r\n";
                    echo "Results: empty" . "\r\n";
                }
            }

            $queryDictionary = [];
            $termString = '';
            echo "QueryOr" . "\r\n";
            foreach ((array)$termRow as $term) {
                $term = trim($term);
                $termString = $termString . $term . ' ';
                if (isset($this->dictionary[$term])) {
                    $queryDictionary[$term] = $this->dictionary[$term];
                }
            }
            echo $termString . "\r\n";
            $uniques = $this->getUniqueQueryIndexes($termRow);
            $this->getQueryIndexWords($uniques, $queryDictionary);
            $tfidfArray = $this->processTFIDF();
            asort($tfidfArray);
            $tfidfArray = array_reverse($tfidfArray, true);

            $mergedArray = call_user_func_array('array_merge', $queryDictionary);

            $mergedArrayIds = [];
            foreach ($mergedArray as $key => $id) {
                $mergedArrayIds[] = (int)$id;
            }

            sort($mergedArrayIds);

            $mergedArrayIds = array_unique($mergedArrayIds);
            $mergedString = '';

            foreach ($mergedArrayIds as $id) {
                $mergedString = $mergedString . $id . ' ';
            }

            echo "Results: $mergedString \r\n";

            echo "TF-IDF \r\n";
            $tfidfString = '';
            foreach ($tfidfArray as $index => $value) {
                if (in_array($index, $mergedArrayIds)) {
                    $tfidfString = $tfidfString . $index . ' ';
                }
            }
            echo "Results: $tfidfString \r\n";

            echo "\r\n";
        }
        $this->wildCard();
    }

    public function handleManualTerms(string $inputTerms): void
    {
        if (empty($inputTerms)) {
            die("No terms were provided!");
        }

        $terms = explode(" ", $inputTerms);

        $this->query = $terms;
    }

    public function handleDictionary(array $documentContent): void
    {
        $termsAndIndexes = [];
        foreach ($documentContent as $document) {
            $termsAndIndexes[] = preg_split("/\t+/", $document);
        }

        $globalArray = [];
        foreach ($termsAndIndexes as $row) {
            $rowIndex = $row[0];
            $rowOfTerms = $row[1];
            $termArray = explode(" ", $rowOfTerms);
            foreach ($termArray as $term) {
                $term = trim($term);
                $globalArray[$term][] = $rowIndex;
                if ($term == "") {
                    continue;
                }
                $this->document[$rowIndex][] = trim($term);

            }
        }

        $this->documentCount = count($termsAndIndexes);
        $this->dictionary = $globalArray;
    }

    public function handleQuery(array $termQueryArray): void
    {
        $query = [];
        foreach ($termQueryArray as $termQuery) {
            $query[] = explode(" ", $termQuery);
        }

        $this->query = $query;
    }

    public function handleBigramQuery(array $bigramQueryArray): void
    {
        $bigrams = [];
        foreach($bigramQueryArray as $key => $value){
            $bigrams = explode(" ", $value);
        }

        $this->bigram = $bigrams;
    }

    public function handleWildCardQuery(array $wildCardQueryArray): void
    {
        $wildcards = [];
        foreach ($wildCardQueryArray as $key => $value) {
            $wildcards[] = trim($value);
        }

        $this->wildcard = $wildcards;
    }

    private function TF(string $word, int $index): float
    {
        $wordAppearanceCount = array_count_values($this->document[$index])[$word];
        $rowLength = count($this->document[$index]);

        return $wordAppearanceCount / $rowLength;
    }

    private function IDF(string $word): float
    {
        return $this->documentCount / count($this->dictionary[$word]);
    }

    public function getUniqueQueryIndexes(array $queryRow): array
    {

        $dictionary = [];
        foreach ($queryRow as $term) {
            $term = trim($term);
            if (isset($this->dictionary[$term])) {
                $dictionary[] = $this->dictionary[$term];
            }
        }

        $indexList = array_values($dictionary);
        $uniqueIndexes = [];
        foreach ($indexList as $key => $indexes) {
            foreach ($indexes as $index) {
                $uniqueIndexes[] = $index;
            }
        }

        return array_unique($uniqueIndexes);
    }

    public function getQueryIndexWords(array $uniqueQueryIndexes, array $dictionary): void
    {
        $arr = [];
        foreach ($uniqueQueryIndexes as $unique) {
            foreach ($dictionary as $word => $numbers) {
                foreach ($numbers as $number) {
                    if ($number == $unique) {
                        $arr[$number][] = $word;
                    }
                }
            }
        }

        $this->queryIndexWords = $arr;
    }

    public function processTFIDF(): array
    {
        $tfIdf = [];
        foreach ($this->queryIndexWords as $index => $words) {
            $tfIdf[$index] = 0;
            foreach ($words as $word) {
                $tfIdf[$index] += $this->TF($word, $index) * $this->IDF($word);
            }
        }

        return $tfIdf;
    }

    public function biGram(): void
    {
        $tempDictionary = [];
        echo "BiGram \r\n";
        echo implode(" ", $this->bigram) . "\r\n";
        echo "Results: \r\n";
        foreach ($this->bigram as $bigram) {
            foreach ((array)$this->dictionary as $term => $postings) {
                if (strpos(trim($term), trim($bigram)) !== false) {
                    $tempDictionary[trim($term)] = $postings;

                }
            }
        }
        foreach ($tempDictionary as $term => $postings) {
            echo "$term \r\n";
            echo "Postings: " . implode(" ", $postings) . "\r\n";
        }
    }

    public function wildCard(): void
    {
        foreach ($this->wildcard as $wildcard) {
            echo "WildCard \r\n";

            echo "$wildcard \r\n";

            $foundWords = [];
            $regexParts = '^';
            $keyWords = explode('*', trim($wildcard));
            foreach ($keyWords as $keyword) {
                if (!empty($keyword)) {
                    $regexParts = $regexParts . sprintf('(?=.*%s)', $keyword);
                }
            }

            foreach ((array)$this->dictionary as $term => $postings) {
                $term = trim($term);
                if (preg_match("/($regexParts)/", $term)) {
                    $foundWords[$term] = $postings;
                }
            }

            echo "Results: \r\n";

            foreach ($foundWords as $term => $postings) {
                echo "$term \r\n";
                echo "Postings: " . implode(" ", $postings) . "\r\n";
            }

            echo "\r\n";
        }
    }
}