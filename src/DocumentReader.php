<?php


namespace src;


class DocumentReader
{
    protected array $documentContents;

    private const TXT_EXTENSION = '.txt';

    public function readDocumentContents(string $fileName)
    {
        $this->documentContents = [];

        $fileName = $this->processTextExtensions($fileName);

        $documentResource = fopen($fileName, 'r');

        if (!$documentResource) {
            die('Document could not be open!');
        } else {
            while (!feof($documentResource)) {
                $this->documentContents[] = fgets($documentResource);
            }

            fclose($documentResource);
        }
    }

    public function saveResults(string $filename, IndexHandler $invertedIndex)
    {
        $filename = $this->processTextExtensions($filename);

        $resultFile = fopen($filename, "w");

        if (empty($invertedIndex->dictionary)) {
            die('Dictionary is empty, please provide text!');
        }

        foreach ($invertedIndex->query as $termRow) {
            fwrite($resultFile, 'Get Postings' . "\r\n");
            foreach ((array)$termRow as $term) {
                fwrite($resultFile, trim($term) . "\r\n");
                $term = trim($term);
                if (isset($invertedIndex->dictionary[$term])) {
                    $wordIndexRow = '';
                    foreach ($invertedIndex->dictionary[$term] as $index) {
                        $wordIndexRow = $wordIndexRow . " $index";
                    }

                    fwrite($resultFile, 'Postings: ' . $wordIndexRow . "\r\n");
                }
            }
            fwrite($resultFile, "\r\n");
        }

        echo 'File saved in the ' . $filename . ' successfully!';
        fclose($resultFile);
    }

    public function saveAndQuery(string $filename, IndexHandler $invertedIndex)
    {
        $filename = $this->processTextExtensions($filename);

        $resultFile = fopen($filename, "w");

        if (empty($invertedIndex->dictionary)) {
            die('Dictionary is empty, please provide text!');
        }

        foreach ($invertedIndex->query as $termRow) {
            $queryDictionary = [];
            $termString = '';
            fwrite($resultFile, "QueryAnd" . "\r\n");
            foreach ((array)$termRow as $term) {
                $term = trim($term);
                $termString = $termString . $term . ' ';
                if (isset($invertedIndex->dictionary[$term])) {
                    $queryDictionary[$term] = $invertedIndex->dictionary[$term];
                }
            }
            fwrite($resultFile, $termString . "\r\n");
            if (count($queryDictionary) == 1) {
                foreach ($queryDictionary as $documentId) {
                    fwrite($resultFile, "Results: " . implode($documentId) . "\r\n");
                    break;
                }
                continue;
            } else {
                $intersectedDictionary = call_user_func_array('array_intersect', $queryDictionary);

                if (!empty($intersectedDictionary)) {
                    $intersectedIdCollectionString = '';
                    foreach ($intersectedDictionary as $intersectedId) {
                        $intersectedIdCollectionString = $intersectedIdCollectionString . $intersectedId . ' ';
                    }
                    fwrite($resultFile, "Results: $intersectedIdCollectionString" . "\r\n");
                } else {
                    fwrite($resultFile, "Results: empty" . "\r\n");
                }
            }
        }

        echo 'File saved in the ' . $filename . ' successfully!';
        fclose($resultFile);

    }

    public function saveOrQuery(string $filename, IndexHandler $invertedIndex)
    {
        $filename = $this->processTextExtensions($filename);

        $resultFile = fopen($filename, "w");

        if (empty($invertedIndex->dictionary)) {
            die('Dictionary is empty, please provide text!');
        }

        foreach ($invertedIndex->query as $termRow) {
            $queryDictionary = [];
            $termString = '';
            fwrite($resultFile, "QueryOr" . "\r\n");
            foreach ((array)$termRow as $term) {
                $term = trim($term);
                $termString = $termString . $term . ' ';
                if (isset($invertedIndex->dictionary[$term])) {
                    $queryDictionary[$term] = $invertedIndex->dictionary[$term];
                }
            }
            fwrite($resultFile, $termString . "\r\n");

            $mergedArray = call_user_func_array('array_merge', $queryDictionary);

            $mergedArrayIds = [];
            foreach ($mergedArray as $key => $id) {
                $mergedArrayIds[] = (int)$id;
            }

            sort($mergedArrayIds);
            $mergedArrayIds = array_unique($mergedArrayIds);
            $mergedString = '';
            foreach ($mergedArrayIds as $id) {
                $mergedString = $mergedString . $id . ' ';
            }

            fwrite($resultFile, "Results: $mergedString \r\n");
        }

        echo 'File saved in the ' . $filename . ' successfully!';
        fclose($resultFile);
    }

    public function saveEverything(string $filename, IndexHandler $invertedIndex)
    {
        $filename = $this->processTextExtensions($filename);

        $resultFile = fopen($filename, "w");

        if (empty($invertedIndex->dictionary)) {
            die('Dictionary is empty, please provide text!');
        }

        foreach ($invertedIndex->query as $termRow) {
            fwrite($resultFile, 'Get Postings' . "\r\n");
            foreach ((array)$termRow as $term) {
                fwrite($resultFile, trim($term) . "\r\n");
                $term = trim($term);
                if (isset($invertedIndex->dictionary[$term])) {
                    $wordIndexRow = '';
                    foreach ($invertedIndex->dictionary[$term] as $index) {
                        $wordIndexRow = $wordIndexRow . " $index";
                    }
                    fwrite($resultFile, 'Postings list : ' . $wordIndexRow . "\r\n");
                }
            }

            $queryDictionary = [];
            $termString = '';
            fwrite($resultFile, "QueryAnd" . "\r\n");
            foreach ((array)$termRow as $term) {
                $term = trim($term);
                $termString = $termString . $term . ' ';
                if (isset($invertedIndex->dictionary[$term])) {
                    $queryDictionary[$term] = $invertedIndex->dictionary[$term];
                }
            }
            $uniqueValues = $invertedIndex->getUniqueQueryIndexes($termRow);
            $invertedIndex->getQueryIndexWords($uniqueValues, $queryDictionary);
            $tfIdfArray = $invertedIndex->processTFIDF();
            asort($tfIdfArray);
            $tfIdfArray = array_reverse($tfIdfArray, true);
            fwrite($resultFile, $termString . "\r\n");

            if (count($queryDictionary) == 1) {
                foreach ($queryDictionary as $documentId) {
                    fwrite($resultFile, "Results: " . implode($documentId) . "\r\n");
                    fwrite($resultFile, "TF-IDF \r\n");
                    fwrite($resultFile, "Results: " . implode($documentId) . "\r\n");
                    break;
                }
            } else {
                $intersectedDictionary = call_user_func_array('array_intersect', $queryDictionary);

                if (!empty($intersectedDictionary)) {
                    $intersectedIdCollectionString = '';
                    foreach ($intersectedDictionary as $intersectedId) {
                        $intersectedIdCollectionString = $intersectedIdCollectionString . $intersectedId . ' ';
                    }
                    fwrite($resultFile, "Results: $intersectedIdCollectionString" . "\r\n");
                    fwrite($resultFile, "TF-IDF \r\n");
                    $tfidfString = '';
                    foreach ($tfIdfArray as $index => $value) {
                        if (in_array($index, $intersectedDictionary)) {
                            $tfidfString = $tfidfString . $index . ' ';
                        }
                    }
                    fwrite($resultFile, "Results: $tfidfString" . "\r\n");

                } else {
                    fwrite($resultFile, "Results: empty" . "\r\n");
                    fwrite($resultFile, "TF-IDF" . "\r\n");
                    fwrite($resultFile, "Results: empty" . "\r\n");
                }
            }

            $queryDictionary = [];
            $termString = '';
            fwrite($resultFile, "QueryOr" . "\r\n");
            foreach ((array)$termRow as $term) {
                $term = trim($term);
                $termString = $termString . $term . ' ';
                if (isset($invertedIndex->dictionary[$term])) {
                    $queryDictionary[$term] = $invertedIndex->dictionary[$term];
                }
            }
            fwrite($resultFile, $termString . "\r\n");

            $uniques = $invertedIndex->getUniqueQueryIndexes($termRow);
            $invertedIndex->getQueryIndexWords($uniques, $queryDictionary);
            $tfidfArray = $invertedIndex->processTFIDF();
            asort($tfidfArray);
            $tfidfArray = array_reverse($tfidfArray, true);

            $mergedArray = call_user_func_array('array_merge', $queryDictionary);

            $mergedArrayIds = [];
            foreach ($mergedArray as $key => $id) {
                $mergedArrayIds[] = (int)$id;
            }

            sort($mergedArrayIds);
            $mergedArrayIds = array_unique($mergedArrayIds);
            $mergedString = '';
            foreach ($mergedArrayIds as $id) {
                $mergedString = $mergedString . $id . ' ';
            }

            fwrite($resultFile, "Results: $mergedString \r\n");

            fwrite($resultFile, "TF-IDF" . "\r\n");

            $tfidfString = '';
            foreach ($tfidfArray as $index => $value) {
                if (in_array($index, $mergedArrayIds)) {
                    $tfidfString = $tfidfString . $index . ' ';
                }
            }
            fwrite($resultFile, "Results: $tfidfString \r\n");


            fwrite($resultFile, "\r\n");
        }

        foreach ($invertedIndex->wildcard as $wildcard) {
            fwrite($resultFile, "WildCard \r\n");
            fwrite($resultFile, "$wildcard \r\n");

            $foundWords = [];
            $regexParts = '^';
            $keyWords = explode('*', trim($wildcard));
            foreach ($keyWords as $keyword) {
                if (!empty($keyword)) {
                    $regexParts = $regexParts . sprintf('(?=.*%s)', $keyword);
                }
            }

            foreach ((array)$invertedIndex->dictionary as $term => $postings) {
                $term = trim($term);
                if (preg_match("/($regexParts)/", $term)) {
                    $foundWords[$term] = $postings;
                }
            }

            fwrite($resultFile, "Results: \r\n");

            foreach ($foundWords as $term => $postings) {
                fwrite($resultFile, "$term \r\n");
                fwrite($resultFile, "Postings: " . implode(" ", $postings) . "\r\n");
            }

            fwrite($resultFile, "\r\n");
        }

        echo 'File saved in the ' . $filename . ' successfully!';
        fclose($resultFile);
    }

    public function getDocumentContent(): array
    {
        return $this->documentContents;
    }

    /**
     * @param string $fileName
     * @return string
     */
    private function processTextExtensions(string $fileName): string
    {
        if (strpos($fileName, self::TXT_EXTENSION) == false) {
            return $fileName . self::TXT_EXTENSION;
        } else {
            return $fileName;
        }
    }




}