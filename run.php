<?php
include 'src/IndexService.php';

echo 'CHOOSE OPTION:' . "\r\n";
echo '1: Get postings from query file' . "\r\n";
echo '2: Get postings manually' . "\r\n";
echo '3: Execute only QueryAnd method' . "\r\n";
echo '4: Execute only QueryOr method' . "\r\n";
echo '5: Get Postings|AND|OR.' . "\r\n";

$option = readline('Enter your option: ');

if (!is_numeric($option)) {
    die('INVALID VALUE FORMAT: Please write only numeric values, you have written a string!');
}

(new \src\IndexService())->handleOption($option);


